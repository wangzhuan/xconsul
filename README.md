# xconsul

## 环境准备
1. 本机部署consul agent，和 nginx，可参考文档：[服务发现consul开发环境配置文档](https://xconfluence.xiaoniangao.cn/wiki/pages/viewpage.action?pageId=73633865)

## 使用方法
1. 服务启动时，调用服务注册方法：

```go
package main

import (
	"fmt"

	"xgit.xiaoniangao.cn/xngo/lib/xconsul/helper"
	"xgit.xiaoniangao.cn/xngo/lib/xconsul/xagent"
)

func registerDemo() {

	// 获取本机ip
	ipAddress, err := xagent.GetLocalIPAddress()
	if err != nil {
		panic(err)
	}
	// 服务注册配置
	serviceConf := xagent.DefaultServiceConf("server-xng", ipAddress, 7001)

	/** 参数视具体情况调整
	serviceConf.HealthCheckPath = "/health"
	serviceConf.HealthCheckInterval = time.Second
	serviceConf.HealthCheckTimeout = time.Second
	*/

	// 服务注册
	err = xagent.RegisterService(serviceConf)
	if err != nil {
		panic(err)
	}
}
```

2. 服务关闭时，调用去掉注册方法

```go
package main

import "xgit.xiaoniangao.cn/xngo/lib/xconsul/xagent"

func deRegisterDemo(serviceID string) {
	//服务取消注册
	//此方法可不调，假如服务不主动调用取消注册，服务挂掉以后，consul会探测到，不会再转发流量
	err := xagent.DeRegisterService(serviceID)
	if err != nil {
		panic(err)
	}
}
```

3. 调用其他服务

```go
package main

import (
	"fmt"
	"xgit.xiaoniangao.cn/xngo/lib/xconsul/helper"
)

func getRequestUrl(serviceName, path string) {
	url := helper.GetURLFromServiceName(serviceName, path)
	fmt.Println(url)
}
```
