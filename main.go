package main

import (
	"fmt"
	"gitlab.com/wangzhuan/xconsul/discover/discoverlog"
	"time"

	"gitlab.com/wangzhuan/xconsul/helper"
	"gitlab.com/wangzhuan/xconsul/xagent"
)

func main() {
	//registerDemo()
	//deRegisterDemo("test")
	//getRequestURL("", "")
	callWithConsulDirect()
}

func registerDemo() {

	// 获取本机ip
	ipAddress, err := xagent.GetLocalIPAddress()
	if err != nil {
		panic(err)
	}
	// 服务注册配置
	serviceConf := xagent.DefaultServiceConf("test_nnn_no_use", ipAddress, 7001)

	/** 参数视具体情况调整
	serviceConf.HealthCheckPath = "/health"
	serviceConf.HealthCheckInterval = time.Second
	serviceConf.HealthCheckTimeout = time.Second
	*/

	// 服务注册
	err = xagent.RegisterService(serviceConf)
	if err != nil {
		panic(err)
	}
}

func deRegisterDemo(serviceID string) {
	//服务取消注册
	//此方法可不调，假如服务不主动调用取消注册，服务挂掉以后，consul会探测到，不会再转发流量
	err := xagent.DeRegisterService(serviceID)
	if err != nil {
		panic(err)
	}
}

func getRequestURL(serviceName, path string) {
	url := helper.GetURLFromServiceName(serviceName, path)
	fmt.Println(url)
}

// 从 consul 获取到服务调用地址
// 如果从 consul 没有拿到，则走原来的 nginx 转发
func callWithConsulDirect() {
	var index int
	for {
		url := helper.GetURLFromServiceName("xng-server", "/health")
		discoverlog.InfoW("get service addr", map[string]interface{}{
			"index":      index,
			"xng-server": url,
		})
		index++
		time.Sleep(time.Second)

		// 测试日志打印控制
		if index > 10 {
			// 日志打印级别控制
			discoverlog.SetLogLevel(discoverlog.LogLevelWarn)
		}

		if index > 15 {
			// 重新放开日志打印级别
			discoverlog.SetLogLevel(discoverlog.LogLevelInfo)
		}
	}
}
