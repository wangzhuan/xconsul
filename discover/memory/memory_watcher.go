package memory

import (
	"errors"
	"gitlab.com/wangzhuan/xconsul/discover/consul"
)

type memWatcher struct {
	id   string
	wo   consul.WatchOptions
	res  chan *consul.Result
	exit chan bool
}

func (m *memWatcher) Next() (*consul.Result, error) {
	for {
		select {
		case r := <-m.res:
			if len(m.wo.Service) > 0 && m.wo.Service != r.Service.Name {
				continue
			}
			return r, nil
		case <-m.exit:
			return nil, errors.New("watcher stopped")
		}
	}
}

func (m *memWatcher) Stop() {
	select {
	case <-m.exit:
		return
	default:
		close(m.exit)
	}
}
