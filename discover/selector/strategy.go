package selector

import (
	"gitlab.com/wangzhuan/xconsul/discover/consul"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// Random is a random strategy algorithm for node selection
func Random(services []*consul.Service) Next {
	nodes := make([]*consul.Node, 0, len(services))

	for _, service := range services {
		nodes = append(nodes, service.Nodes...)
	}

	return func() (string, error) {
		if len(nodes) == 0 {
			return "", ErrNoneAvailable
		}

		i := rand.Int() % len(nodes)
		return nodes[i].Address, nil
	}
}
