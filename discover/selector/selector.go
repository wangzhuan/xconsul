// Package selector is a way to pick a list of service nodes
package selector

import (
	"errors"
	"gitlab.com/wangzhuan/xconsul/discover/consul"
)

// Selector builds on the registry as a mechanism to pick nodes
// and mark their status. This allows host pools and other things
// to be built using various algorithms.
type Selector interface {
	Init(opts ...Option) error
	Options() Options
	// Select returns a function which should return the next node
	Select(service string) (string, error)
	// Mark sets the success/error against a node
	String() string
}

// Next is a function that returns the next node
// based on the selector's strategy
type Next func() (string, error)

// Strategy is a selection strategy e.g random, round robin
type Strategy func([]*consul.Service) Next

var (
	ErrNotFound      = errors.New("not found")
	ErrNoneAvailable = errors.New("none available")
)
