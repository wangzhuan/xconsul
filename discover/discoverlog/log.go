package discoverlog

import (
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Logger interface {
	DebugW(msg string, fields map[string]interface{})
	InfoW(msg string, fields map[string]interface{})
	WarnW(msg string, fields map[string]interface{})
	ErrorW(msg string, fields map[string]interface{})
	FatalW(msg string, fields map[string]interface{})
}

type RLogLevel uint8

const (
	LogLevelDebug RLogLevel = iota
	LogLevelInfo
	LogLevelWarn
	LogLevelError
	LogLevelFatal
)

var rLog Logger
var rLogLevel = LogLevelInfo // 日志打印级别, 默认 info

func init() {
	level := os.Getenv("REGISTRY_GO_LOG_LEVEL")

	// 设置日志级别
	atomicLevel := zap.NewAtomicLevel()
	atomicLevel.UnmarshalText([]byte(level))

	zws := make([]zapcore.WriteSyncer, 0)

	// 输出控制台
	zws = append(zws, zapcore.AddSync(os.Stdout))

	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(makeEncoderConfig()),
		zapcore.NewMultiWriteSyncer(zws...),
		atomicLevel,
	)

	zapOptions := make([]zap.Option, 0)

	//开启开发模式，堆栈跟踪
	caller := zap.AddCaller()
	skip := zap.AddCallerSkip(2)
	zapOptions = append(zapOptions, caller, skip)

	logger := zap.New(core, zapOptions...)
	sugar := logger.Sugar()

	r := &defaultLogger{
		logger: sugar,
	}

	rLog = r
}

func makeEncoderConfig() zapcore.EncoderConfig {
	return zapcore.EncoderConfig{
		MessageKey:       "msg",
		LevelKey:         "level",
		TimeKey:          "time",
		NameKey:          zapcore.OmitKey,
		CallerKey:        "path",
		FunctionKey:      "func",
		StacktraceKey:    "stack",
		LineEnding:       zapcore.DefaultLineEnding,
		EncodeLevel:      zapcore.LowercaseLevelEncoder, // 小写编码器
		EncodeTime:       currentTimeEncoder,
		EncodeDuration:   zapcore.SecondsDurationEncoder, //
		EncodeCaller:     zapcore.ShortCallerEncoder,     // 全路径编码器
		EncodeName:       zapcore.FullNameEncoder,
		ConsoleSeparator: "\t",
	}
}

func currentTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	t = t.Local()
	enc.AppendString(t.Format("2006-01-02 15:04:05"))
}

type defaultLogger struct {
	logger *zap.SugaredLogger
}

func (l *defaultLogger) DebugW(msg string, fields map[string]interface{}) {
	if msg == "" && len(fields) == 0 {
		return
	}
	l.logger.Debugw(msg, getKVs(fields)...)
}

func (l *defaultLogger) InfoW(msg string, fields map[string]interface{}) {
	if msg == "" && len(fields) == 0 {
		return
	}
	l.logger.Infow(msg, getKVs(fields)...)
}

func (l *defaultLogger) WarnW(msg string, fields map[string]interface{}) {
	if msg == "" && len(fields) == 0 {
		return
	}
	l.logger.Warnw(msg, getKVs(fields)...)
}

func (l *defaultLogger) ErrorW(msg string, fields map[string]interface{}) {
	if msg == "" && len(fields) == 0 {
		return
	}
	l.logger.Errorw(msg, getKVs(fields)...)
}

func (l *defaultLogger) FatalW(msg string, fields map[string]interface{}) {
	if msg == "" && len(fields) == 0 {
		return
	}
	l.logger.Fatalw(msg, getKVs(fields)...)
}

func getKVs(m map[string]interface{}) []interface{} {
	var keysAndValues []interface{}

	for k, v := range m {
		keysAndValues = append(keysAndValues, k)
		keysAndValues = append(keysAndValues, v)
	}
	return keysAndValues
}

// SetLogger use specified logger user customized, in general, we suggest user to replace the default logger with specified
func SetLogger(logger Logger) {
	rLog = logger
}

// 设置日志打印级别
func SetLogLevel(level RLogLevel) {
	rLogLevel = level
}

func DebugW(msg string, fields map[string]interface{}) {
	if rLogLevel > LogLevelDebug {
		return
	}
	rLog.DebugW(msg, fields)
}

func InfoW(msg string, fields map[string]interface{}) {
	if rLogLevel > LogLevelInfo {
		return
	}
	if msg == "" && len(fields) == 0 {
		return
	}
	rLog.InfoW(msg, fields)
}

func WarnW(msg string, fields map[string]interface{}) {
	if rLogLevel > LogLevelWarn {
		return
	}
	if msg == "" && len(fields) == 0 {
		return
	}
	rLog.WarnW(msg, fields)
}

func ErrorW(msg string, fields map[string]interface{}) {
	if rLogLevel > LogLevelError {
		return
	}
	rLog.ErrorW(msg, fields)
}

func FatalW(msg string, fields map[string]interface{}) {
	if rLogLevel > LogLevelFatal {
		return
	}
	rLog.FatalW(msg, fields)
}
