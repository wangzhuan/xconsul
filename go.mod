module gitlab.com/wangzhuan/xconsul

go 1.12

require (
	github.com/armon/go-metrics v0.3.6 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/uuid v1.3.0
	github.com/hashicorp/consul/api v1.8.1
	github.com/hashicorp/go-hclog v0.14.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.3.0 // indirect
	github.com/hashicorp/go-msgpack v0.5.5 // indirect
	github.com/hashicorp/go-sockaddr v1.0.2 // indirect
	github.com/hashicorp/go-uuid v1.0.2 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/miekg/dns v1.1.41 // indirect
	github.com/mitchellh/go-testing-interface v1.14.0 // indirect
	github.com/mitchellh/hashstructure v1.1.0
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/pkg/errors v0.8.1
	go.uber.org/zap v1.19.0
	golang.org/x/net v0.0.0-20210410081132-afb366fc7cd1 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
