package helper

import (
	"fmt"
	"gitlab.com/wangzhuan/xconsul/discover/consul"
	"gitlab.com/wangzhuan/xconsul/discover/selector"
	"gitlab.com/wangzhuan/xconsul/xagent"
	"os"
	"strings"
)

var (
	nginxAddr       = "127.0.0.1"
	agentAddr       = "127.0.0.1:8500"
	defaultSelector selector.Selector
)

func init() {
	k8sNodeIP := os.Getenv("K8S_NODE_IP")
	k8sNodeIP = strings.TrimSpace(k8sNodeIP)

	if k8sNodeIP != "" {
		nginxAddr = k8sNodeIP
		agentAddr = k8sNodeIP + ":8500"
	}

	defaultSelector = selector.NewSelector(selector.Registry(consul.NewRegistry(
		consul.Addrs(agentAddr),
	)))
}

// GetURLFromServiceName 获取目标服务的请求地址
// targetServiceName可以是ip、域名
// 兼容 k8s 环境部署，从 consul 获取到服务调用地址
// 如果从 consul 没有拿到，则走原来的 nginx 转发
func GetURLFromServiceName(targetServiceName, path string) string {
	var (
		format string
		addr   string
		err    error
	)

	//不是validService，默认为ip或者域名，如果写ip的话，请在ip后面带上端口
	if !xagent.IsValidServiceName(targetServiceName) {
		if strings.HasPrefix(path, "/") {
			format = "http://%s%s"
		} else {
			format = "http://%s/%s"
		}
	} else if addr, err = defaultSelector.Select(targetServiceName); err == nil {
		// consul 直连
		if strings.HasPrefix(path, "/") {
			format = "http://%s%s"
		} else {
			format = "http://%s/%s"
		}

		return fmt.Sprintf(format, addr, path)

	} else {
		// nginx
		if strings.HasPrefix(path, "/") {
			format = "http://" + nginxAddr + ":9090/%s%s"
		} else {
			format = "http://" + nginxAddr + ":9090/%s/%s"
		}
	}

	return fmt.Sprintf(format, targetServiceName, path)
}
