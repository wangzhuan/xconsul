package helper

import "testing"

func TestGetURLFromServiceName(t *testing.T) {
	t.Log(GetURLFromServiceName("server-xng", "user/info"))
	t.Log(GetURLFromServiceName("server-xng", "/user/info"))

	t.Log(GetURLFromServiceName("192.168.101.6", "/user/info"))
	t.Log(GetURLFromServiceName("192.168.101.6:7001", "user/info"))

}
