package xagent

import (
	"errors"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

// ErrGetNoIP 获取IP失败
var ErrGetNoIP = errors.New("get no ip")

const (
	// DefaultHealthCheckPath 默认健康检查路径
	DefaultHealthCheckPath = "/health"
	// DefaultCheckInterval 默认健康检查间隔时间
	DefaultCheckInterval = 1 * time.Second
	// DefaultCheckTimeout 默认健康检查超时时间
	DefaultCheckTimeout = 600 * time.Millisecond
)

// ServiceConf 注册服务，相关的配置
type ServiceConf struct {
	ServiceID   string            //ServiceID，建议 serviceName + Port
	ServiceName string            //服务名，只能有英文字母+数字，以及 - 组成
	Tags        []string          // 服务标签
	Port        int               // 端口
	Address     string            //ip地址，建议127.0.0.1
	Meta        map[string]string //允许写一些meta信息

	HealthCheckPath     string        //健康检查地址，默认 /health
	HealthCheckInterval time.Duration //健康检查 间隔时间, ms
	HealthCheckTimeout  time.Duration //健康检查 超时时间, ms

	ConsulAddress string //consul address 默认127.0.0.1:8500
}

// GetHostName 获取本机的hostname，如果获取失败，返回随机Int
func GetHostName() (hostName string, err error) {
	hostName, err = os.Hostname()
	if hostName == "" {
		hostName = strconv.Itoa(rand.Int())
	}
	return
}


// getLocalIPAddress 获取本机ip地址，获取失败，返回127.0.0.1
func getLocalIPAddress() (ipAddress string, err error) {
	var addrs []net.Addr
	addrs, err = net.InterfaceAddrs()
	if err != nil {
		return "127.0.0.1", err
	}
	for _, a := range addrs {
		if ipNet, ok := a.(*net.IPNet); ok && !ipNet.IP.IsLoopback() {
			if ipNet.IP.To4() != nil {
				ipAddress = ipNet.IP.String()
				return
			}
		}
	}
	if ipAddress == "" {
		ipAddress = "127.0.0.1"
		err = ErrGetNoIP
	}

	return
}


// GetLocalIPAddress 获取本机ip地址，获取失败，返回127.0.0.1
func GetLocalIPAddress() (ipAddress string, err error) {
	ipAddress, err = getLocalIPAddress()
	k8sNodeIP := os.Getenv("K8S_NODE_IP")
	k8sNodeIP = strings.TrimSpace(k8sNodeIP)
	if k8sNodeIP != "" {
		ipAddress = k8sNodeIP
	}
	return
}

// DefaultServiceConf default服务注册对象
//func DefaultServiceConf(serviceName, address string, port int) *ServiceConf {
func DefaultServiceConf(serviceName, ipAddress string, port int) *ServiceConf {
	//var idPost string
	//ipAddr, err := GetLocalIPAddress()
	//if err != nil {
	//	idPost, err = GetHostName()
	//	if err != nil {
	//		fmt.Println("get hostName error", err)
	//	}
	//} else {
	//	idPost = ipAddr
	//}
	return &ServiceConf{
		ServiceID:           serviceName + "-" + ipAddress,
		ServiceName:         serviceName,
		Tags:                []string{},
		Port:                port,
		Address:             ipAddress,
		Meta:                map[string]string{},
		HealthCheckPath:     DefaultHealthCheckPath,
		HealthCheckInterval: DefaultCheckInterval,
		HealthCheckTimeout:  DefaultCheckTimeout,
	}
}
