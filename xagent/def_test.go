package xagent

import (
	"testing"
)

func TestGetHostName(t *testing.T) {
	hostName, err := GetHostName()
	if err != nil {
		t.Error(err)
	}
	t.Log(hostName)
}

func TestGetIPAddress(t *testing.T) {
	gotIPAddress, err := GetLocalIPAddress()
	if err != nil {
		t.Errorf("GetLocalIPAddress() error = %v", err)
		return
	}
	t.Log(gotIPAddress)
}

func TestDefaultServiceConf(t *testing.T) {
	ip, _ := GetLocalIPAddress()
	t.Log(DefaultServiceConf("xng-server-222", ip, 7001))
}
