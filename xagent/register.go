package xagent

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/hashicorp/consul/api"
	"github.com/pkg/errors"
)

// ErrServiceNameNotValid serviceName不合法错误
var ErrServiceNameNotValid = errors.New("serviceName can only contain 0-9,a-z,A-Z and -")

// IsValidServiceName 判断serviceName是否合法
func IsValidServiceName(serviceName string) bool {
	if serviceName == "" {
		return false
	}
	isValid := true
	for _, v := range serviceName {
		if (v >= '0' && v <= '9') || (v >= 'a' && v <= 'z') || v >= 'A' && v <= 'Z' || v == '-' || v == '_' {
			continue
		}
		isValid = false
		break
	}
	return isValid
}

// RegisterService 服务注册。注册重试3次，注册失败，请调用方处理。
func RegisterService(serviceConf *ServiceConf) error {
	if serviceConf == nil {
		return errors.New("nil serviceConf")
	}
	if !IsValidServiceName(serviceConf.ServiceName) {
		return ErrServiceNameNotValid
	}
	var err error
	for i := 0; i < 3; i++ {
		err = registerServiceOneTime(serviceConf)
		if err == nil {
			return nil
		}
		<-time.After(1 * time.Second)
	}
	return err
}

// DeRegisterService 服务取消注册。注册重试3次。
func DeRegisterService(serviceID string) error {
	var err error
	for i := 0; i < 3; i++ {
		err = deRegisterServiceOneTime(serviceID)
		if err == nil {
			return nil
		}
		<-time.After(1 * time.Second)
	}
	return err
}

func deRegisterServiceOneTime(serviceID string) error {
	config := api.DefaultConfig()
	client, err := api.NewClient(config)
	if err != nil {
		fmt.Printf("【error】consul client error : %v\n", err)
		return err
	}
	err = client.Agent().ServiceDeregister(serviceID)
	if err != nil {
		fmt.Printf("【error】deRegister server error :%v\n", err)
		return err
	}

	return nil
}

func registerServiceOneTime(serviceConf *ServiceConf) error {
	config := api.DefaultConfig()
	if serviceConf.ConsulAddress != "" {
		config.Address = serviceConf.ConsulAddress
	}
	client, err := api.NewClient(config)
	if err != nil {
		fmt.Printf("【error】consul client error : %v\n", err)
		return err
	}
	//创建一个新服务。
	registration := new(api.AgentServiceRegistration)
	registration.ID = serviceConf.ServiceID
	registration.Name = serviceConf.ServiceName
	registration.Port = serviceConf.Port
	registration.Tags = serviceConf.Tags
	registration.Address = serviceConf.Address
	if len(serviceConf.Meta) > 0 {
		registration.Meta = serviceConf.Meta
	}

	//增加check。
	check := new(api.AgentServiceCheck)
	var format string
	if strings.HasPrefix(serviceConf.HealthCheckPath, "/") {
		format = "http://%s:%d%s"
	} else {
		format = "http://%s:%d/%s"
	}
	check.HTTP = fmt.Sprintf(format, registration.Address, registration.Port, serviceConf.HealthCheckPath)
	//设置超时 5s。
	check.Timeout = strconv.FormatInt(int64(serviceConf.HealthCheckTimeout/time.Millisecond), 10) + "ms"
	//设置间隔 5s。
	check.Interval = strconv.FormatInt(int64(serviceConf.HealthCheckInterval/time.Millisecond), 10) + "ms"
	//注册check服务。
	registration.Check = check

	err = client.Agent().ServiceRegister(registration)

	if err != nil {
		fmt.Printf("【error】register server error :%v\n", err)
		return err
	}

	return nil
}
