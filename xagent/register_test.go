package xagent

import "testing"

func Test_isServiceNameValid(t *testing.T) {
	t.Log(IsValidServiceName("xng-server"))
	t.Log(IsValidServiceName("xng_server"))
	t.Log(IsValidServiceName("xng11server"))
	t.Log(IsValidServiceName("xng11server.srv"))
}

func TestRegisterService(t *testing.T) {
	ip, _ := GetLocalIPAddress()
	serviceConf := DefaultServiceConf("bd.log.222", ip, 7001)
	err := RegisterService(serviceConf)
	t.Log(err)
}

func TestDeRegisterService(t *testing.T) {
	err := DeRegisterService("bd.log.222-192.168.6.31")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("success")
}
